class FiscalDateHelper
    attr_accessor :today, :fiscal_months, :fiscal_quarters, :fiscal_month_index, :fiscal_quarter, :fiscal_year

    def initialize(today)
        @today = today
        @fiscal_months      = [2,3,4,5,6,7,8,9,10,11,12,1]
        @fiscal_quarters    = [1,2,3,4]
        @fiscal_month_index = set_fiscal_month_index
        @fiscal_quarter     = set_fiscal_quarter
        @fiscal_year        = set_fiscal_year
        @this_quarter       = this_quarter
        @next_quarter       = next_quarter
    end

    def set_fiscal_month_index
        @fiscal_months.find_index(@today.month)
    end

    def set_fiscal_quarter
        @fiscal_quarters[@fiscal_month_index / 3]
    end

    def set_fiscal_year
        @today.month == 1 ? @today.strftime("%y").to_i : @today.strftime("%y").to_i + 1
    end

    def this_quarter
        "FY#{@fiscal_year} Q#{@fiscal_quarter}"
    end

    def next_quarter
        "FY#{@fiscal_quarter == 4 ? @fiscal_year + 1 : @fiscal_year} Q#{@fiscal_quarter == 4 ? 1 : @fiscal_quarter + 1}"
    end

    #TODO
    
    def first_date_of_this_quarter
    end
        
    def last_date_of_this_quarter
    end

    def first_date_of_next_quarter
    end
    
    def last_date_of_next_quarter
    end

    def first_date_of_last_quarter
    end

    def last_date_of_last_quarter
    end

end
