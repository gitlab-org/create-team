Release planning for the [Code Review Group](https://about.gitlab.com/handbook/product/product-categories/#code-review-group)

## Overview - [<MILESTONE> Release Board](https://gitlab.com/groups/gitlab-org/-/boards/2159734?scope=all&utf8=%E2%9C%93&label_name%5B%5D=group%3A%3Acode%20review&milestone_title=<MILESTONE>)

## Backend

### Planning distribution

* ~"type::feature": 
* ~"type::maintenance": 
* ~"type::bug": 

### Priorities

1. 
1. 
1. 

## Frontend

### Planning distribution

* ~"type::feature": 
* ~"type::maintenance": 
* ~"type::bug": 

### Priorities

1. 
1. 
1. 

## Issues scheduled for <MILESTONE>

<details><summary>Unassigned issues ~backend / ~frontend  (should be empty by the end of planning)</summary>

```glql
display: table
fields: epic, title, assignee, weight, labels("type::*", "backend", "frontend")
query:
  group = "gitlab-org" and label = "group::code review"
  and label in ("backend", "cli", "frontend") and label != "type::ignore"
  and milestone = "<MILESTONE>" and opened = true
  and assignee = none
```

</details>

<details><summary>Scheduled issues (sort by epic) ~backend</summary>

```glql
display: table
fields: epic, title, assignee, weight, labels("type::*", "Deliverable", "Stretch")
query:
  group = "gitlab-org" and label = "group::code review"
  and label in ("backend", "cli") and label != "type::ignore"
  and milestone = "<MILESTONE>" and opened = true
```

</details>

<details><summary>Scheduled issues (sort by epic) ~frontend</summary>

```glql
display: table
fields: epic, title, assignee, weight, labels("type::*")
query:
  group = "gitlab-org" and label = "group::code review"
  and label in ("frontend") and label != "type::ignore"
  and milestone = "<MILESTONE>" and opened = true
```

</details>

## Slippage from <PREVIOUS_MILESTONE>

<details><summary>Open ~backend issues from <PREVIOUS_MILESTONE></summary>

```glql
display: table
fields: health, assignee, weight, labels("backend-weight::*", "workflow::*"), labels( "Deliverable", "Stretch"), title
query:
  group = "gitlab-org" and opened = true and milestone = "<PREVIOUS_MILESTONE>"
  and assignee in ("garyh", "kinsingh", "marc_shaw", "patrickbajao", "dskim_gitlab") 
```

</details>

 
<details><summary>Open ~frontend issues from <PREVIOUS_MILESTONE></summary>

```glql
display: table
fields: health, assignee, weight, labels("frontend-weight::*", "workflow::*"), labels( "Deliverable", "Stretch"), title
query:
  group = "gitlab-org" and opened = true and milestone = "<PREVIOUS_MILESTONE>"
  and assignee in ("iamphill", "slashmanov", "thomasrandolph") 
```

</details>

## Quick wins

<details><summary>Weight 1 ~backend issues</summary>

```glql
display: table
fields: assignee, weight, title, milestone, updatedAt
query: group = "gitlab-org" and label = "group::code review" and label = "backend" and weight = 1 and opened = true
```

</details>

<details><summary>Weight 1 ~cli issues</summary>

```glql
display: table
fields: assignee, weight, title, milestone, updatedAt
query: project = "gitlab-org/cli" and label = "group::code review" and weight = 1 and opened = true
```

</details>

<details><summary>Weight 1 ~frontend issues</summary>

```glql
display: table
fields: assignee, weight, title, milestone, updatedAt
query: group = "gitlab-org" and label = "group::code review" and label = "frontend" and weight = 1 and opened = true
```

</details>

<details><summary>~"quick win" issues</summary>

```glql
display: table
fields: assignee, weight, title, milestone, updatedAt
query: group = "gitlab-org" and label = "group::code review" and label = "quick win"  and opened = true
```

</details>

## Stable counterparts

- [ ] Technical Writing (@aqualls)
- [ ] Quality (@jay_mccure)

## Staff+ engineers

All engineers are welcome to contribute to the planning process!
Staff+ engineers are _expected_ to contribute, see references below,
and are therefore tagged here explicitly to ensure visiblity as soon as the issue is created.
- ~backend: @patrickbajao
- ~frontend: @iamphill

Handbook references:
- [Staff+ archetype - Tech Lead](https://handbook.gitlab.com/handbook/engineering/ic-leadership/#tech-lead)
  - "A Staff Engineer partners with the Engineering Manager and the Product Manager for milestone planning"
- [Staff+ archetype - Right hand](https://handbook.gitlab.com/handbook/engineering/ic-leadership/#right-hand)
  - "Staff+ Engineers are supposed to broaden the perspectives of their managers. Decision-makers often need the additional context and perspective to make well-informed decisions about investments in the product architecture, understanding expected ROI"
- [Development Staff career framework](https://handbook.gitlab.com/handbook/engineering/careers/matrix/development/staff/#staff-values-alignment)
  - "Works with the Engineering Manager to assign work to the team"

## Missing labels / weights

%<MILESTONE> issues without a ~backend / ~frontend / ~cli label (list should be empty):

```glql
display: table
fields: assignee, weight, title
query:
  group = "gitlab-org" and label in ("group::code review") and milestone = "<MILESTONE>"
  and label != "type::ignore" and opened = true
  and label != "frontend" and label != "backend" and label != "cli"
```

%<MILESTONE> issues without a weight (list should be empty):

```glql
display: table
fields: assignee, labels("backend", "frontend"), weight, labels("backend-weight::*", "frontend-weight::*"), title
query:
  group = "gitlab-org" and label in ("group::code review") and milestone = "<MILESTONE>"
  and label != "type::ignore" and opened = true
  and weight = None
```

---

([improve this template?](https://gitlab.com/gitlab-org/create-stage/-/blob/master/.gitlab/issue_templates/code-review-planning.md))

/label ~"group::code review" ~"devops::create" ~"section::dev" ~"Planning Issue" ~"type::ignore"
/assign @phikai @francoisrose @andr3 @mle
/milestone %<MILESTONE>
