<!--

Please update:
WEEKDAY
MONTH
DAY
YEAR
DEADLINE_FOR_SUGGESTIONS — at least 1 week before the Team Day

Perform checklist at the bottom

-->

## What is this?

Hello team!

It's time to have another Create Team Day! (See the [list of past team days](https://gitlab.com/gitlab-org/create-stage/-/issues?label_name[]=Team%20Day)).

Please use: [#s_create_team-day](https://gitlab.slack.com/archives/C012PDJJ8QG) on Slack.

#### **Event Goal:** have a long time socializing with your fellow teammates in the ~"devops::create" stage for one day!

* We recommend **blocking out any work-related activities and meetings** so that you have the entire day to socialize.
* Apart from the scheduled timeslots, you are **encouraged to schedule your own ad-hoc initiatives** like playing specific games. 

## When

**Date:** WEEKDAY, MONTH DAY YEAR

**Time:** Throughout the day!

### Timeslots

To increase the attendance and allow every team member to participate, we'll have several meetings along the day to socialize.

Proposed 1h timeslots:

| Auckland 🇳🇿 | Melbourne 🇦🇺  | Manila 🇵🇭 | Delhi 🇮🇳 | London 🇬🇧 | New York 🇺🇸  | Vancouver 🇨🇦 |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| **1pm** | **11am** | **9am** | 6:30am | 2am | 9pm ❖ | *6pm* ❖ | 
| **4pm** | **2pm** | **12pm** | **9:30am** | 5am | 12am | 9pm ❖ | 
| 8pm | *6pm* | **4pm** | **1:30pm** | **9am** | 4am | 1am | 
| 1am ☨ | 11pm | 9pm | *6:30pm* | **2pm** | **9am** | 6am | 
| 4am ☨ | 2am ☨ | 12am ☨ | 9:30pm | **5pm** | **12pm** | **9am** | 
| 8am ☨ | 6am ☨ | 4am ☨ | 1:30am ☨ | 9pm | **3pm** | **1pm** | 

* **bold** = within working hours
* *italic* = a bit early/a bit late
* ☨ = Saturday, outside of working works.
* ❖ = Thursday, outside of working works.

## Where

Events for each timeslot will be in the **Create Stage calendar** ([Add to Google Calendar](https://calendar.google.com/calendar/b/2?cid=Z2l0bGFiLmNvbV9ydGNlajZlcjJhNW45bjlya28yMWNvZXY0NEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) or subscribe to `gitlab.com_rtcej6er2a5n9n9rko21coev44@group.calendar.google.com`). There are Zoom meeting URLs attached to each event.

## What activities?

It's up to us to decide, really, so we'll open a thread in this issue for you to throw your suggestions until the end of **DEADLINE_FOR_SUGGESTIONS**. We'll have a bit over a week to vote on the proposed activities for each slot.

## Anything else we missed?

Ask in the comments and we'll get to you!

## Checklist for the organizer

### Initial Setup

* [ ] Create an issue with the title: `Create Team Day - Nth Edition - WEEKDAY, MONTH DAY YEAR`
* [ ] Create all 6 events on [Create Stage Google Calendar](https://calendar.google.com/calendar/b/2?cid=Z2l0bGFiLmNvbV9ydGNlajZlcjJhNW45bjlya28yMWNvZXY0NEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t). **See January 29th, 2021 examples.** To make this process a bit faster, you can create all the events at once by using Google calendar's "import CSV" feature. [Here is a sample spreadhsheet](https://docs.google.com/spreadsheets/d/16ZwiRhv2ccudwGXydEtcE1uNfDjGxO3ezKW0FxPG15Q/edit?usp=sharing) you can use to create the events.
    * [ ] Create a new Zoom room that is open to participants all day
    * [ ] Add zoom room link to the event
    * [ ] Add the agenda to the event description: https://docs.google.com/document/d/14vVXNm8lLU06N9b0EasKcQpO4zRvODQRYQCC2KMowPw/edit
* [ ] Create a thread in this issue to collect suggestions of activities until DEADLINE_FOR_SUGGESTIONS
* [ ] Share the link to the issue in #s_create and #s_create_team-day and ask for suggestions

### On DEADLINE_FOR_SUGGESTIONS

* [ ] Create several Google Forms, one for each session. Like [this one](https://docs.google.com/forms/d/e/1FAIpQLSeKp6wI8swZfxgLV63mIvauCjuFsR5n809kjgdbCJJJWzrwTQ/viewform). <!-- if you want to clone it, ask any of the previous organizers on Slack to add you as a collaborator so you can clone the form -->
* [ ] Share the links to the forms in #s_create_team-day and in this issue

### 1 Day Before Create Team Day

* [ ] Fill in the top 3 voted activites for each session in:
    * [ ] Agenda
    * [ ] Google Calendar events



/label ~"Team Day"
