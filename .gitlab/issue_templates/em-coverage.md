## Intent
This issue describes how broader group and team members are [supporting an Engineering Manager in taking a meaningful break away from work](https://about.gitlab.com/handbook/support/support-time-off.html#support-team-member-time-off). The intent is that the EM isn't required to "catch up" before and after taking a well-deserved vacation. It is also an opportunity for EMs and team members to collaborate with others outside their immediate group.

This issue is an extension of the [Engineering Manager Backup](https://about.gitlab.com/handbook/engineering/management/#engineering-manager-backup) process described in the handbook.

## :handshake: Responsibilities
Include creating release planning, weekly triage issues, responding to technical requests, team retros, team member 1:1s etc.

| Priority | Theme | Context Link | Primary | Secondary |
| -------- | ----- | ------------ | ------- | --------- |



## :muscle: Coverage Tasks


## :book: References



## :white_check_mark: Issue Tasks

### :o: Opening Tasks
- [ ] Assign to yourself
- [ ] Title the issue `EM Coverage for YOUR-NAME from DD-MON-YYYY until DD-MON-YYYY`
- [ ] Add an issue comment for your :recycle: Retrospective Thread
- [ ] Add any relevant references including direction pages, group handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your section, stage and group Slack channels
- [ ] Ensure you've assigned tasks via PTO Ninja
- [ ] Ensure your PTO Ninja auto-responder points team members to this issue
- [ ] Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/#current-status) to include a reference to this issue

### :x: Closing Tasks
- [ ] Update your GitLab status to indicate you are back at work. Your Gmail and Slack statuses should automatically update.
- [ ] Consider thanking your backup for their support.
- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective Items and update [this template](https://gitlab.com/gitlab-org/create-stage/-/blob/master/.gitlab/issue_templates/em-coverage.md)
